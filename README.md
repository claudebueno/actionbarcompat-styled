# ActionBar Compat Styled

Cet exemple vous montre comment utiliser ActionBarCompat avec un thème personnalisé.

Il utilise une barre d'action fractionnée lors de l'exécution sur un périphérique avec un affichage étroit et affiche trois onglets.


![Screeshot](https://gitlab.com/claudebueno/actionbarcompat-styled/raw/master/screenshots/Screenshot_1528555164.png)

.
